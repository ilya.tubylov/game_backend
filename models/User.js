const {Schema, model} = require('mongoose')

const User = new Schema({
    login: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    email: {type: String, unique: true, required: true},
    gold: {type: Number, default: 1000}
})

module.exports = model('User', User)