const express = require('express')
const path = require('path')
const cors = require('cors')
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {secret} = require("./config")
const PORT = 9001
const URL_DB = 'mongodb+srv://admin:1334@cluster0.csdgt.mongodb.net/?retryWrites=true&w=majority'
const User = require('./models/User')

const app = express()

app.use(cors())
app.use(express.json())

// app.use(express.static(__dirname + "/static"))

// app.get('/news', (req, res) => {
//     res.sendFile(path.join(__dirname, './static/index.html'));
// })

const generateAccessToken = (id) => {
    const payload = {
        id
    }
    return jwt.sign(payload, secret, {expiresIn: "24h"} )
}

app.get('/', (req, res) => {
    res.send('<h1>Ура, у нас получилось!</h1>');
})
app.get('/user/:name', (req, res) => {
    res.send(`<h1>Пользователь: ${req.params.name}</h1>`);
})
app.post('/login', async (req, res) => {
    const {login, password} = req.body
    const user = await User.findOne({login})
    if(!user){
        return res.json({message: 'Пользователь не найден'})
    }
    const validPassword = bcrypt.compareSync(password, user.password)
    if (!validPassword) {
        return res.status(400).json({message: `Введен неверный пароль`})
    }
    const token = generateAccessToken(user._id)
    return res.status(200).json({
        token: token,
        message: `Добро пожаловать`
    })
})
app.post('/registration', async (req, res) => {
    const {login, password, email} = req.body
    const user = await User.findOne({login})
    if(user){
        return res.json({message: 'Пользователь уже зарегистрирован'})
    }
    const hashPassword = bcrypt.hashSync(password, 7)
    const person = new User({login, password: hashPassword, email})
    await person.save()
    res.json({
        message: 'Пользователь успешно зарегистрирован',
        login: login,
        password: password,
        email: email
    })
})



const start = async () => {
    try {
        await mongoose.connect(URL_DB, { useNewUrlParser: true, useUnifiedTopology: true, authSource:"admin" })
        app.listen(PORT, () => console.log(`Сервер запущен на ${PORT} порте`))
    } catch (e) {
        console.log(e)
    }
}

start()